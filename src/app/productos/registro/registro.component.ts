import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ProductoServicioService } from '../producto-servicio.service';
import { Producto } from '../producto.interface';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  formularioRegistraProducto: FormGroup;

  constructor(private fbuild: FormBuilder, private productoS:ProductoServicioService) { }

  ngOnInit(): void {
    this.crearFormulario();
  }
/*
.......##.......##....########..########...#######..########..##.....##..######..########..#######.
......##.......##.....##.....##.##.....##.##.....##.##.....##.##.....##.##....##....##....##.....##
.....##.......##......##.....##.##.....##.##.....##.##.....##.##.....##.##..........##....##.....##
....##.......##.......########..########..##.....##.##.....##.##.....##.##..........##....##.....##
...##.......##........##........##...##...##.....##.##.....##.##.....##.##..........##....##.....##
..##.......##.........##........##....##..##.....##.##.....##.##.....##.##....##....##....##.....##
.##.......##..........##........##.....##..#######..########...#######...######.....##.....#######.
*/

// aca creamos un producto
  crearFormulario() {
    this.formularioRegistraProducto = this.fbuild.group({
      nombre: ['', [Validators.required]],
      descripcion: ['', [Validators.required]],
      urlImg: ['', [Validators.required]],
      precio: ['', [Validators.required]],
      marca: ['', [Validators.required]],
    });
  }

  //función para el registro de un usuario
/*
.......##.......##.########..########..######...####..######..########.########...#######.
......##.......##..##.....##.##.......##....##...##..##....##....##....##.....##.##.....##
.....##.......##...##.....##.##.......##.........##..##..........##....##.....##.##.....##
....##.......##....########..######...##...####..##...######.....##....########..##.....##
...##.......##.....##...##...##.......##....##...##........##....##....##...##...##.....##
..##.......##......##....##..##.......##....##...##..##....##....##....##....##..##.....##
.##.......##.......##.....##.########..######...####..######.....##....##.....##..#######.
*/

//registro
  registrar() {
    const productito: Producto = this.formularioRegistraProducto.value;
    this.productoS.registrarProducto(productito).subscribe(res =>{
      console.log(res);
    })
  }

}
